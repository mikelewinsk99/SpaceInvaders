package server;

import java.lang.reflect.Array;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ServerSpaceInvaders extends Thread{

    private ServerSocket ss = null;
    private BufferedReader in = null;
    private PrintWriter out = null;

    private boolean running = true;

    private int scoreboardSize = 5;

    public void standDown(){this.running = false;}
    public void standBy(){this.running = true;}

    String rootPath;

    public ServerSpaceInvaders(String host, int port) {
        rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        try {
            InetSocketAddress adress = new InetSocketAddress(host, port);
            this.ss = new ServerSocket();             // Gniazdo serwera
            ss.bind(adress);
            System.out.println("Server started");
            System.out.println("at port: " + ss.getLocalPort());
            System.out.println("bind address: " + ss.getInetAddress());
        }catch(Exception e) {
            e.printStackTrace();
            System.exit(5);
        }
        start();
    }

    public void run(){

        System.out.println("tada: "+rootPath);

        while (running){
            try {
                Socket socket = ss.accept();  // czeka na nastęne połączenie
                System.out.println("Serwer: nawiązałem połączenie");
                handleRequest(socket); // obsługa zlecenia

            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }                               // zamknięcie gniazda serwera
        try { ss.close(); } catch (Exception exc) {}
    }


    // protokół używa spacji do dzielenia słów i żadna linijka nie przekracza 2 słów
    private static Pattern requestPattern = Pattern.compile(" ", 2);



    // Obsługa zleceń od klienta
    private void handleRequest(Socket connection) throws IOException {
        try {
            in = new BufferedReader(                   // utworzenie strumieni
                    new InputStreamReader(
                            connection.getInputStream()));
            out = new PrintWriter(
                    connection.getOutputStream(), true);
            System.out.println("Serwer: Wchodzę do pętli");
            // wczytaj komendę
            String line = in.readLine();
            System.out.println("Serwer: odczytuję ->"+ line);

                if (line != null) {
                    String[] req = requestPattern.split(line, 2); // rozbiór zlecenia
                    System.out.println(req[0]+" "+req[1]);
                    String cmd = req[0];//komenda
                    String version = req[1];//wersja gry klienta
                    switch(cmd){

                        case "submitScore":
                            System.out.println(rootPath+"server/versions/"+version+"/score.txt");

                            ArrayList<String[]> scores = new ArrayList<>();
                            Properties serverScores = new Properties();
                            try{
                                serverScores.load(new FileInputStream(rootPath+"server/versions/"+version+"/score.txt"));
                            } catch(Exception e){
                                out.println("error");
                            }

                            for(int i = 1; i<=scoreboardSize;i++){
                                String[] accomplished = serverScores.getProperty(Integer.toString(i)).split(";",3);
                                scores.add(accomplished);
                            }

                            String[] splitLine;
                            while(!(line = in.readLine()).equals("*")){
                                System.out.println(line);
                                splitLine = line.split("=",2)[1].split(";",3); //utnij "=" i wczytaj scoresy

                                boolean playerExists=false; int place;
                                for(int i = 0 ; i <scores.size();i++){    //ochrona przed nadpisaniem identycznych wyników
                                    if(splitLine[2].equals(scores.get(i)[2])  && splitLine[1].equals(scores.get(i)[1]) && splitLine[0].equals(scores.get(i)[0]) ){
                                        playerExists=true; place=i; break;
                                    }
                                }
                                if(!playerExists)
                                scores.add(splitLine);
                            }

                            //magia stabilnego sortowania
                            Collections.sort(scores,new Comparator<String[]>() {
                                public int compare(String[] strings, String[] otherStrings) {
                                    if(Integer.parseInt(strings[1]) < Integer.parseInt(otherStrings[1]))
                                        return 1;
                                    else
                                        return -1;
                                }
                            });
                            Collections.sort(scores,new Comparator<String[]>() {
                                public int compare(String[] strings, String[] otherStrings) {
                                    return (-1)*strings[0].compareTo(otherStrings[0]);
                                }
                            });

                            for(int i = 0 ; i <scores.size();i++)
                                System.out.println(scores.get(i)[0]+";"+scores.get(i)[1]+";"+scores.get(i)[2]);

                            for(int i = 0; i<scoreboardSize;i++) //od pierwszego do ostatniego liczonego miejsca
                            {
                                serverScores.setProperty(Integer.toString(i+1),scores.get(i)[0]+";"+scores.get(i)[1]+";"+scores.get(i)[2]);
                            }

                            serverScores.store(new FileOutputStream(rootPath+"server/versions/"+version+"/score.txt"), null); //nadpisz nasz plik

                            //wyślij go tak jak w getScore
                            try (BufferedReader br = new BufferedReader(new FileReader(rootPath+"server/versions/"+version+"/score.txt"))) {
                                String line1;
                                br.readLine(); //pierwsza linijka pliku to #data (fix)
                                out.println("confirmScore "+version);
                                while ((line = br.readLine()) != null) {
                                    System.out.println("wczytuję " + line);
                                    out.println(line);
                                }
                                out.println("*");
                            }
                            catch(IOException ioException){
                                out.println("error");
                            }

                            break;

                        case "getScore":
                            System.out.println(rootPath+"server/versions/"+version+"/score.txt");
                            try (BufferedReader br = new BufferedReader(new FileReader(rootPath+"server/versions/"+version+"/score.txt"))) {
                                String line1;
                                br.readLine(); //pierwsza linijka pliku to #data (fix)
                                out.println("sendScore "+version);
                                while ((line = br.readLine()) != null) {
                                    System.out.println("wczytuję " + line);
                                    out.println(line);
                                    //rows.add(values);
                                }
                                out.println("*");
                            }
                            catch(IOException ioException){
                                out.println("error");
                            }


                            break;

                        default:
                            out.println("error");
                            break;
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Przetwarzanie żądania coś trafiło");
        } finally {
            try {
                in.close();                        // sprząta po połączeniu (na 1 porcie 1 wpis = 1 połączenie żeby nie blokować)
                out.close();
                connection.close();
                connection = null;
                System.out.println("Connection close");
            } catch (Exception exc) { }
        }
    }


    public static void main(String[] args) throws IOException {
        new ServerSpaceInvaders("localhost",1000);
    }
}

