package temporary;

public class TestRunnable implements Runnable{

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */

    public void TestRunnable(){

    }

    @Override
    public void run() {
        int i = 0;
        while(true){
            System.out.println("jestem zwyciezca"+i + " " + Thread.currentThread());i++;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void innaMetoda(){
        while(true){
            System.out.println("jestem przegrywem.." + " "+ Thread.currentThread());
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]) {

        TestRunnable tester = new TestRunnable();
        Thread t = new Thread(tester);
        t.start();

        tester.innaMetoda();

    }

}
