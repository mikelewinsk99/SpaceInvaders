import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class TestClass {


    public static void main(String[] args) throws Exception {
        ArrayList<String[]> listOfStringArrays = new ArrayList<String[]>();
        listOfStringArrays.add(new String[] {"A","3000","z"});
        listOfStringArrays.add(new String[] {"B","100","c"});
        listOfStringArrays.add(new String[] {"A","3000","z"});
        listOfStringArrays.add(new String[] {"C","200","z"});
        listOfStringArrays.add(new String[] {"A","35000","z"});
        listOfStringArrays.add(new String[] {"D","3000","z"});
        listOfStringArrays.add(new String[] {"A","2000","z"});
        listOfStringArrays.add(new String[] {"A","1000","z"});
        listOfStringArrays.add(new String[] {"C","200","o"});
        Collections.sort(listOfStringArrays,new Comparator<String[]>() {
            public int compare(String[] strings, String[] otherStrings) {
                if(Integer.parseInt(strings[1]) < Integer.parseInt(otherStrings[1]))
                    return 1;
                else
                    return 0;
            }
        });
        Collections.sort(listOfStringArrays,new Comparator<String[]>() {
            public int compare(String[] strings, String[] otherStrings) {
                return (-1)*strings[0].compareTo(otherStrings[0]);
            }
        });
        for (String[] sa : listOfStringArrays) {
            System.out.println(Arrays.toString(sa));
        }
        /* prints out
          [a, b, c]
          [m, n, o]
          [x, y, z]
        */

    }
}
