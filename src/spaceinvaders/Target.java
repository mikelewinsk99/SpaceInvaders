package spaceinvaders;

import java.awt.*;
import javax.swing.*;

/**
 * Podstawowa klasa trafialnego obiektu i zniszczalnego obiektu
 */

public class Target {

    GamePanel game;

    private int hitpoints;
    private int pos_x;
    private int pos_y;

    private Dimension board;


    private int protrusionHorizontal=10;
    private int protrusionVertical=10;
    private int type;
    private int bounty = 0;

    public int getHitpoints(){return this.hitpoints;}
    public void setHitpoints(int hp){this.hitpoints = hp;}

    public void setPos_x(int x){this.pos_x = x;}
    public void setPos_y(int y){this.pos_x = y;}
    public void setXY(int x, int y){this.pos_x = x;this.pos_y = y;}
    public int getPos_x(){return this.pos_x;}
    public int getPos_y(){return this.pos_y;}

    /**
     * Metoda przesuwająca cel o kwant odległości w osi poziomej
     * @param direction kierunek
     */

    public void moveOnXAxis(boolean direction){
        if(direction)
            pos_x++;
        else
            pos_x--;
    }

    /**
     * Metoda przesuwająca cel o kwant odległości w osi pionowej
     * @param dropDown kierunek
     */
    public void moveOnYAxis(boolean dropDown) {
        if(dropDown)
            pos_y=pos_y+protrusionVertical;
    }

    public void setProtrusionHorizontal(int d){this.protrusionHorizontal = d;}
    public void setProtrusionVertical(int d){this.protrusionVertical =d;}
    public int getProtrusionHorizontal(){return protrusionHorizontal;}
    public int getProtrusionVertical(){return protrusionVertical;}

    public void setType(int t){this.type = t;}
    public int getType(){return this.type;}

    public void setBounty(int b){this.bounty = b;}
    public int getBounty(){return this.bounty;}


    public Target(GamePanel game,int x, int y, Dimension b){
        this.board=b;
        this.pos_x=x;
        this.pos_y=y;
    }

    /**
     * Metoda wołana przez wątki pocisku w momencie stwierdzenia trafienia
     * Odejmują od celu określoną liczbę HP równą uszkodzeniom pocisku, a następnie
     * zwraca informację, czy trafiony cel został zniszczony czy uszkodzony
     * @param damage obrażenia pocisku, który wywołał metodę
     * @return true - cel nie ma prawa przeżyć trafienia, false - celowi zostały jeszcze życia
     */

    public boolean attemptToDestroy(int damage){
        this.hitpoints = this.hitpoints - damage;
        if(this.hitpoints<=0)
            return true;
        else
            return false;
    }

    /**
     * Metoda wołana z paint() służąca wyrysowaniu trafialnego obiektu, kolor w zależności od jego typu
     * @param g2D
     * @param scaledFrom rozmiar planszy
     * @param scaledTo rozmiar graficznej reprezentacji panelu rozgrywki
     */

    public void drawSelf(Graphics2D g2D, Dimension scaledFrom, Dimension scaledTo){
        switch(this.type){
            case 0:
                g2D.setPaint(Color.white);
                break;
            case 1:
                g2D.setPaint(Color.yellow);
                break;

            case 2:
                g2D.setPaint(Color.orange);
                break;

            case 3:
                g2D.setPaint(Color.red);
                break;
            case 4:
                g2D.setPaint(Color.green);
                break;
            case 5:
                g2D.setPaint(Color.lightGray);
            default:
        }

        int topLeftX = pos_x - protrusionHorizontal, topLeftY = pos_y - protrusionVertical;
        int bottomRightX = pos_x + protrusionHorizontal, bottomRightY = pos_y + protrusionVertical;
        double scaleX = ((double)scaledTo.width)/((double)scaledFrom.width);   //proporcje planszy gry |int^2| do graficznego okna gry |pixel^2|
        double scaleY = ((double)scaledTo.height)/((double)scaledFrom.height);
        g2D.fillRect((int)(topLeftX*scaleX),(int)(topLeftY*scaleY),(int)(2*protrusionHorizontal*scaleX),(int)(2*protrusionVertical*scaleY));
    }

}
