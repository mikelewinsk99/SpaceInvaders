package spaceinvaders;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Klasa runnable służąca dodawaniu wątków spadających
 * bomb na koordynatach losowo bybranego obcego z formacji
 */

public class BombGenerator implements Runnable {

    private GamePanel game;
    private Enemy[] enemies;
    private Bomb[] bombs;
    int numberOfBombs, activeBombs;
    private Player player;

    private final Object lock = new Object();
    public Object getLock(){return lock;}


    public BombGenerator(GamePanel game) {
        this.game = game;
        this.player = this.game.player;
        this.enemies = this.game.enemies;

        numberOfBombs= Integer.parseInt(this.game.level.getProperty("numberOfBombs"));
        activeBombs=0;
        bombs = new Bomb[numberOfBombs];


        //System.out.println("przed rozpoczeciem watku pocisku");
        Thread t = new Thread(this);
        t.start();
    }

    public Bomb[] getBombs(){return bombs;}

    public void terminateBombs(){
        for(int i=0;i<bombs.length;i++)
        {
            bombs[i]=null;
        }
        activeBombs = 0;
    }

    @Override
    public void run() {


        boolean hasHit = false;

        GameClock tickSignal = GameClock.getInstance();

        Rectangle self;
        Rectangle hitbox;

        //System.out.println("pojawiam sie na wysokosci "+pos_y);

        while (this.game.gameIsOn) {
            synchronized (tickSignal) {
                try {
                    tickSignal.wait();
                    //System.out.println("Generator tick!!!");
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            if(activeBombs>=numberOfBombs){
                //System.out.println("wchodzę do blokady bomb");
                synchronized(bombs){
                    try {
                        bombs.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            if(activeBombs<numberOfBombs){
                //System.out.println("generator wchodzę do obliczen");
                try {
                    Random r = new Random();
                    if (r.nextDouble() < 0.05) {
                        ArrayList<Enemy> pick = new ArrayList<>();
                        for (int i = 0; i < enemies.length; i++) {
                            if (enemies[i] != null)
                                pick.add(enemies[i]);
                        }
                        if (pick.size() != 0) {
                            int p = r.nextInt(pick.size());
                            for(int n = 0; n<numberOfBombs;n++){
                                if(bombs[n]==null){
                                    bombs[n] = new Bomb(game, pick.get(p), bombs, n);
                                    activeBombs++;
                                    break;
                                }
                            }
                        }
                    }
                    //System.out.println("generator wychodze z nich!!!!!");
                }catch(Exception e){e.printStackTrace();}
            }

        }
    }
}


