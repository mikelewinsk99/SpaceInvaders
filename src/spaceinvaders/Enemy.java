package spaceinvaders;

import java.awt.*;
import java.util.Properties;

/**
 * Klasa obcego będącego w składzie schodzącej na ziemie formacji
 * @author Michał Lewiński
 */

public class Enemy extends Target{

    Bomb bomb;

    public Enemy(GamePanel game, int x, int y, char c) {
        super(game, x,y,new Dimension(1,1));
        setProtrusionHorizontal(Integer.parseInt(game.core.getProperty("enemy_protrusion_horizontal")));
        setProtrusionVertical(Integer.parseInt(game.core.getProperty("enemy_protrusion_vertical")));
        setType((int)(c-48)); //ASCII
        switch(getType()){
            case 1:
                System.out.println("yeeeet"+ game.core.getProperty("enemyHealth"));
                setHitpoints(Integer.parseInt(game.core.getProperty("enemyHealth")));
                setBounty(Integer.parseInt(game.level.getProperty("enemyBounty")));
                break;
            case 2:
                System.out.println(game.core.getProperty("yeeeeet"+"uberHealth"));
                setHitpoints(Integer.parseInt(game.core.getProperty("uberHealth")));
                setBounty(Integer.parseInt(game.level.getProperty("uberBounty")));
                break;

        }
    }
}
