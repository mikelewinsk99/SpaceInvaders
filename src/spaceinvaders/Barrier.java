package spaceinvaders;

import java.awt.*;

/**
 * Planowana implementacja bariery, obiektu trafialnego zarowno
 * przez pocisk gracza jak i obcych, będąca zniszczalna badz
 * niezniszczalną osłoną dla gracza przed zrzucanymi bombami.
 */

public class Barrier extends Target{


    public Barrier(GamePanel game, int x, int y, Dimension b) {
        super(game, x, y, b);
    }
}
