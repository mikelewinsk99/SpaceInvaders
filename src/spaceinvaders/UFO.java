package spaceinvaders;

import java.awt.*;

/**
 * Planowana implementacja UFO, obiektu swobodnego obiektu obcych
 * poruszającego się poza formacją, szybko wlatującego i wylatującego z pola gry
 * w górnej jego części, którego zabicie jest premiowane.
 */

public class UFO extends Target implements Runnable{
    public UFO(GamePanel game,int x, int y, Dimension b) {
        super(game,x,y,b);
    }


    @Override
    public void run() {

    }
}
