package spaceinvaders;

import java.awt.*;
import java.util.Properties;

/**
 * Klasa pojazdu gracza
 */

public class Player extends Target{

    GamePanel game;

    public Player(GamePanel game, int x, int y, Dimension b){
        super(game, x,y,b);
        this.game = game;
        setProtrusionHorizontal(Integer.parseInt(game.core.getProperty("player_protrusion_horizontal")));
        setProtrusionVertical(Integer.parseInt(game.core.getProperty("player_protrusion_vertical")));
        setHitpoints(Integer.parseInt(game.core.getProperty("playerHealth")));
        setType(0);
    }

}
