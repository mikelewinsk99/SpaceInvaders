package spaceinvaders;


import java.awt.*;

/**
 * Klasa pocisku zrzucanego przez obcych (Enemy), która
 * po trafieniu gracza zadaje mu obrażenia
 */

public class Bomb implements Runnable {

    private int pos_x, pos_y;
    private int damage;
    boolean isDefused;   //

    private GamePanel game;
    private Player player;
    private Enemy enemy;
    private UFO ufo;

    //private BombGenerator bombGen;
    private Bomb[] bombs;
    private int index;

    public Bomb(GamePanel game, Enemy e, Bomb[] bombs, int n) {
        //this.bombGen = bg;
        this.bombs = bombs;
        this.index = n;
        this.game = game;
        this.player = game.player;
        this.enemy = e;
        this.ufo = game.ufo;

        damage = 1;
        isDefused = false;

        this.pos_x = enemy.getPos_x();
        this.pos_y = enemy.getPos_y();

        //System.out.println("wlasnie utworzono bombe!");
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * Metoda wywoływana z paint() służąca naniesieniu obiektu przez kontekst graficzny
     * @param g2D
     * @param scaledFrom wymiary właściwej planszy gry
     * @param scaledTo wymiary panelu gry w oknie aplikacji
     */
    public void drawSelf(Graphics2D g2D, Dimension scaledFrom, Dimension scaledTo) {   //do poprawy ale na razie ok
        if(!isDefused) {
            g2D.setPaint(Color.red);
            int topLeftX = pos_x - 5, topLeftY = pos_y - 5;
            int bottomRightX = pos_x + 5, bottomRightY = pos_y + 5;
            double scaleX = ((double) scaledTo.width) / ((double) scaledFrom.width);   //proporcje planszy gry |int^2| do graficznego okna gry |pixel^2|
            double scaleY = ((double) scaledTo.height) / ((double) scaledFrom.height);

            //System.out.println(scaleX);
            g2D.fillRect((int) (topLeftX * scaleX), (int) (topLeftY * scaleY), (int) (2 * 5 * scaleX), (int) (2 * 5 * scaleY));
        }
    }

    /**
     * Metoda rozbrajająca bomby, służy do sygnalizacji aktywnym wątkom spadających bomb,
     * że gracz został już trafiony, doszło do resetu pocisków i obecne już w powietrzu
     * bomby zostają unieszkodliwione
     */

    public void defuse(){
        this.isDefused=true;
    }

    /**
     *
     */
    @Override
    public void run() {
        boolean hasHit = false;

        GameClock tickSignal = GameClock.getInstance();

        Rectangle self;
        Rectangle hitbox;


        while (!hasHit) {

            synchronized (tickSignal) {
                try {
                    tickSignal.wait();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
            this.pos_y += 3;
            if (this.pos_y > game.board.height) {
                hasHit = true;
                break;
            }
            synchronized(player) {
                if(!isDefused) {
                    Player e = game.player;
                    hitbox = new Rectangle(e.getPos_x() - e.getProtrusionHorizontal(), e.getPos_y() - e.getProtrusionVertical(), 2 * e.getProtrusionHorizontal(), 2 * e.getProtrusionVertical());
                    self = new Rectangle(pos_x - 1, pos_y - 1, 2, 2);
                    if (hitbox.intersects(self)) {
                        hasHit = true;
                        for (int i = 0; i < bombs.length; i++) {
                            if(bombs[i]!=null)
                            bombs[i].defuse();
                        }
                        if (game.player.attemptToDestroy(damage)) {
                            game.gameOver();
                        }
                        game.updateLives();
                        break;

                    }
                }
            }
        }

        synchronized(bombs){
            bombs[index] = null;
            if(game.bombGen!=null)
            game.bombGen.activeBombs--;
            bombs.notifyAll();
        }

    }

}
