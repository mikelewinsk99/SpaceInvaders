package spaceinvaders;

import java.awt.*;
import java.util.Random;

/**
 * Wątek formacji, jego podstawową funkcją jest przemieszcznie
 * standardowych obcych w sposób zgodny z przyjętą konwencją SpaceInvaders
 */

public class Formation implements Runnable {

    GamePanel game;
    Dimension board;
    Enemy[] enemies;
    int enemiesLeft;

    boolean direction;
    boolean dropDown;

    public Formation(GamePanel game, Dimension board) {
        this.game = game;
        this.board = board;
        this.enemies = this.game.enemies;
        enemiesLeft = enemies.length;

        dropDown = false;
        direction = true;

        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {

        GameClock tickSignal = GameClock.getInstance();

        Rectangle bouncerLeft = new Rectangle(0, 0, 1, board.height);

        System.out.println("wymiary boxa" + board.width + " i " + board.height);
        Rectangle bouncerRight = new Rectangle(board.width - 1, 0, board.width, board.height);
        Rectangle hitbox;

        while (enemiesLeft > 0) {

            synchronized (tickSignal) {
                try {
                    tickSignal.wait();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }

            for (int i = 0; i < enemies.length; i++) {
                if (enemies[i] != null) {
                    enemies[i].moveOnXAxis(direction);
                    if (dropDown){
                        enemies[i].moveOnYAxis(dropDown);
                        if(enemies[i].getPos_y()+enemies[i].getProtrusionVertical()> board.height*5/6){
                            System.out.println(enemies[i].getPos_y()+" ? "+board.height);
                            game.gameOver();
                            System.out.println("Doszło do przegranej przez lądowanie - oczekuj na wyjście formacji z run()"); //nie wyjdzie..
                            break;
                        }

                    }

                }
            }
            dropDown = false;

            for (int i = 0; i < enemies.length; i++) {
                if (enemies[i] != null) {
                    Enemy e = enemies[i];
                    hitbox = new Rectangle(e.getPos_x() - e.getProtrusionHorizontal(), e.getPos_y() - e.getProtrusionVertical(), 2 * e.getProtrusionHorizontal(), 2 * e.getProtrusionVertical());
                    if (hitbox.intersects(bouncerLeft) || hitbox.intersects(bouncerRight)) {
                        System.out.println("odbito!");
                        direction = (!direction);  //zmien kierunek X
                        dropDown = true;
                        break;
                    }
                }
            }

        }
        //warunek ukonczenia poziomu to zbicie wszystkich obcych z formacji
            game.completeLevel();
    }

    /**
     * Metoda wołana przez wątek pocisku gracza po wyeliminowaniu obcego, służąca
     * obliczeniu proporcji pozostałych przy życiu obcych, która zostanie użyta
     * do wyliczenia należytego okresu taktowania (prędkości gry)
     */

    public void goAndUpdateTickRate() {
        double alive = 0, all = 0;
        for (int i = 0; i < enemies.length; i++) {
            all++;
            if (enemies[i] != null) {
                alive++;
            }
        }
        System.out.println((enemiesLeft +"kurczebele"+ enemies.length));
        game.updateTickrate(((double)enemiesLeft)/((double)enemies.length));
    }
}