package spaceinvaders;

import java.awt.*;


/**
 * Wątek kuli armatniej czyli pocisku gracza generowanego
 * na żądanie (spacja), jeśli nie znajduje się już w locie
 */

public class CannonBall implements Runnable {

    private int pos_x, pos_y;
    private int damage;
    private int travelSpeed;

    private GamePanel game;
    private Player player;
    private Enemy[] enemies;
    private UFO ufo;


    public CannonBall(GamePanel game) {
        this.game = game;
        this.player = game.player;
        this.enemies = game.enemies;
        this.ufo = game.ufo;
        this.travelSpeed = game.bulletTravelSpeed;

        damage = 1;

        this.pos_x = player.getPos_x();
        this.pos_y = player.getPos_y() - player.getProtrusionVertical();

        //System.out.println("przed rozpoczeciem watku pocisku");
        Thread t = new Thread(this);
        t.start();
    }


    public void drawSelf(Graphics2D g2D, Dimension scaledFrom, Dimension scaledTo) {   //do poprawy ale na razie ok
        g2D.setPaint(Color.white);
        int topLeftX = pos_x - 5, topLeftY = pos_y - 5;
        int bottomRightX = pos_x + 5, bottomRightY = pos_y + 5;
        double scaleX = ((double) scaledTo.width) / ((double) scaledFrom.width);   //proporcje planszy gry |int^2| do graficznego okna gry |pixel^2|
        double scaleY = ((double) scaledTo.height) / ((double) scaledFrom.height);

        //System.out.println(scaleX);
        g2D.fillRect((int) (topLeftX * scaleX), (int) (topLeftY * scaleY), (int) (2 * 5 * scaleX), (int) (2 * 5 * scaleY));
    }


    @Override
    public void run() {
        boolean hasHit = false;

        GameClock tickSignal = GameClock.getInstance();

        Rectangle self;
        Rectangle hitbox;


        while (!hasHit) {

            synchronized (tickSignal) {
                try {
                    tickSignal.wait();
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
                this.pos_y -= travelSpeed;
                if (this.pos_y < 0) {
                    hasHit = true;
                    break;
                }

                for (int i = 0; i < enemies.length; i++) {
                    if (enemies[i] != null) {
                        Enemy e = enemies[i];
                        hitbox = new Rectangle(e.getPos_x() - e.getProtrusionHorizontal(), e.getPos_y() - e.getProtrusionVertical(), 2 * e.getProtrusionHorizontal(), 2 * e.getProtrusionVertical());
                        self = new Rectangle(pos_x - 1, pos_y - 1, 2, 2);
                        if (hitbox.intersects(self)) {
                            hasHit = true;
                            if (enemies[i].attemptToDestroy(damage)){
                                game.formation.enemiesLeft--;
                                game.formation.goAndUpdateTickRate();
                                game.addScore(enemies[i].getBounty());
                                enemies[i] = null;  //usun przeciwnika
                                break;
                            }

                        }
                    }
                }
            }
        //System.out.println("i po petli");
        game.cannonBall = null;
        }

}
