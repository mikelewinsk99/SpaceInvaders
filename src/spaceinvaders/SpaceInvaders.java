package spaceinvaders;


import java.awt.*;

import javax.swing.JFrame;

/**
 * Klasa o nazwie identycznej z samą grą służąca do intuicyjnego
 * wywołania z pliku.bat
 */

public class SpaceInvaders {

    public static void main(String args[]) {

        GameFrame game = new GameFrame();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
    }
}
