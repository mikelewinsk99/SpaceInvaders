package spaceinvaders;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;

/**
 * Klasa MainMenu jest panelem bocznym w oknie aplikacji, wyposażonym w obsługę zdarzeń,
 * przyciski do nawigacji po aplikacji
 * @author Michał Lewiński
 */
public class MainMenu extends JPanel implements ActionListener {

    String host = "localhost";
    int port = 1000;

    GamePanel game;

    JTextField level, score, lives;
    JButton button_play, button_network, button_highscores, button_how_to;
    //static GameFrame game;

    Properties hs = new Properties();

    public MainMenu(GamePanel gp) {
        this.game = gp;

        Dimension forTextfields = new Dimension(400,100);
        Dimension forButtons = new Dimension(190,50);

        setSize(400, 600);
        this.setBackground(Color.darkGray);
        this.setMinimumSize(new Dimension (400,400));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        level = new JTextField("    Level: ");
        //level.setBounds(100,100,100,20);
        level.setForeground(Color.white);
        level.setBackground(Color.darkGray);
        level.setMaximumSize(forTextfields);
        level.setEditable(false);
        add(level);
        score = new JTextField("    Score: ");
        //level.setBounds(100,100,100,20);
        score.setForeground(Color.white);
        score.setBackground(Color.darkGray);
        score.setMaximumSize(forTextfields);
        score.setEditable(false);
        add(score);
        lives = new JTextField("    Lives: ");
        //level.setBounds(100,100,100,20);
        lives.setForeground(Color.white);
        lives.setBackground(Color.darkGray);
        lives.setMaximumSize(forTextfields);
        lives.setEditable(false);
        add(lives);


        button_play = new JButton("Start");
        button_play.setMaximumSize(forButtons);
        add(button_play);
        button_play.addActionListener(this);

        button_highscores = new JButton("Tabela");
        //button_highscores.setBounds(100, 200, 100, 20);
        button_highscores.setMaximumSize(forButtons);
        add(button_highscores);
        button_highscores.addActionListener(this);

        button_network = new JButton("Sieć");
        //button_pause.setBounds(100, 250, 100, 20);
        button_network.setMaximumSize(forButtons);
        add(button_network);
        button_network.addActionListener(this);

        button_how_to = new JButton("Pomoc");
        //button_how_to.setBounds(100, 300, 100, 20);
        button_how_to.setMaximumSize(forButtons);
        add(button_how_to);
        button_how_to.addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source == button_play) {

            game.unpause();
            game.unloadMap(); //wcisniecie start resetuje nam gre bez zapisu
            game.resetScore();

            Object[] opts1 = {"Arcade", "Treningowy"};

            int x = JOptionPane.showOptionDialog(game, "Wybierz tryb gry: ","Start", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opts1, opts1[0]);
            System.out.println(x);
            String[] possibilities = game.core.getProperty("levels").split(";");
            if(x == 1) {                                                                //tryb treningowy tylko 1 lv naraz
                String chosenmap = (String)JOptionPane.showInputDialog(
                        game,
                        "Wybierz poziom:\n",
                        "Treningowy",
                        JOptionPane.PLAIN_MESSAGE,
                        null,
                        possibilities,
                        possibilities[0]);

                game.isRanked=false;
                game.loadMap(chosenmap);
                game.gameOn();
            }
            if(x==0)    //tryb arcade, rozgrywany od pierwszej mapy do ostatniego poziomu lub gameover
            {
                game.setArcadeCurrentLevel(0);
                game.loadMap(possibilities[0]);
                game.isRanked=true;
            }
            game.requestFocus();
        }
        else if( source == button_network) {

            String[] options = new String[] {"Submit Score", "Get Score", "Cancel"};
            String msg = "Możesz pobrać z serwera aktualną listę najlepszych wyników na świecie\n" +
                    "Możesz też wysłać lokalnie zapisane rekordy, może któryś z nich może się mierzyć z najlepszymi..";
            int response = JOptionPane.showOptionDialog(null, msg, "Sieć",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[0]);
            System.out.println(response);
            if(response == 0 || response == 1){
                connectWithServer(game, response);
            }
            //Config instruction=new Config();
            //JOptionPane.showMessageDialog(this, instruction.getInstruction());
        }
        else if( source == button_highscores) {
           JOptionPane.showMessageDialog(game, prepareHighscores());
           game.requestFocus();//szybki fix, bo w stanie pauzy jak kliknie się okno
                                //to wątek gamePanel nie ma jak poprosić o fokus w metodzie run
        }
        else if( source == button_how_to) {
            JOptionPane.showMessageDialog(game, prepareInstruction());
            game.requestFocus();
        }
        else
            System.out.println("Quitting..");

    }

    /**
     * Metoda przygotowująca zawartość pliku highscore.properties do wyświetlenia na oknie dialogowym
     * celem prezentacji rekordów uzyskanych w grze
     * @return
     */


    private String prepareHighscores(){
        try {
            hs.load(new FileInputStream(game.rootPath + "config/highscore/highscore.properties"));
        } catch (IOException e) {e.printStackTrace();}
        String reply = "Najlepsze wyniki: \n";
        for(int i=1;i<=5;i++){
            reply = reply + hs.getProperty(Integer.toString(i)) + "\n";//hs.getProperty(Integer.toString(i)).split(";");
        }
        return reply;
    }


    /**
     * Metoda przygotowująca plik tekstowy instrukcji do wyświetlenia w oknie dialogowym
     * po kliknięciu przycisku "Pomoc"
     * @return
     */
    public String prepareInstruction() {
        File file = new File (game.rootPath + "config/instruction.txt");
        Scanner in =null;
        try {
            in = new Scanner(file);
        }
        catch(Exception e) {
            return null;
        }
        String instruction = null;
        try {
            instruction = ("\n");
            while(true) {
                instruction = instruction+in.nextLine()+("\n");
                if(instruction.equals("#END")) {
                    break;
                }
            }
        } catch(Exception e) {
            try {in.close();} catch (IllegalStateException ise) {return null;}
        }
        return instruction;
    }

    /**
     * Metoda łącząca MainMenu z zewnętrznym serwerem przechowującym rekordy graczy, wołana
     * przez działanie na przycisku "Sieć"
     * Menu ma dostęp do lokalnie przechowywanych plików highscore, dzięki czemu może
     * zsynchronizować lokalne dane z sieciowymi lub wprost zastąpić lokalne sieciowymi
     * @param game właściwa część wykonawcza gry
     * @param option wybrana opcja z panelu dialogowego (submit lub get)
     */
    private void connectWithServer(GamePanel game, int option){
        try {
            Socket socket = new Socket(host, port);
            BufferedReader brFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter pwToServer = new PrintWriter(socket.getOutputStream(), true);

        if(option == 0) { //submit

            System.out.println("jestem tutaj");
            String request = "submitScore 1", eof = "*";
            ArrayList<String> rowsToSend = new ArrayList<>();
            ArrayList<String> rowsReceived = new ArrayList<>();
            try(BufferedReader br = new BufferedReader(new FileReader(game.rootPath+"config/highscore/highscore.properties"))){
                String row;
                br.readLine(); //pierwsza linijka pliku to #data (fix)
                while((row = br.readLine())!= null)  //wczytanie własnych plików
                rowsToSend.add(row);
            }catch(Exception e){
                JOptionPane.showMessageDialog(this, "błąd w odczycie lokalnych plików");
                return;
            }
            pwToServer.println(request);
            for(int i = 0;i<rowsToSend.size();i++){
                pwToServer.println(rowsToSend.get(i));
            }
            pwToServer.println(eof);
            pwToServer.flush();

            String lineToRead = brFromServer.readLine();
            if(!lineToRead.equals("confirmScore 1")){
                JOptionPane.showMessageDialog(this, "błąd nawiazaniu połączenia z serwerem 2");
                return;
            }else{
                while(!(lineToRead=brFromServer.readLine()).equals("*")){
                    rowsReceived.add(lineToRead);
                }
            }

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(game.rootPath+"config/highscore/highscore.properties"))){

                for(int i = 0;i< rowsReceived.size();i++){
                    bw.write(rowsReceived.get(i));
                    bw.newLine();
                }
            }catch(Exception e){
                JOptionPane.showMessageDialog(this, "błąd w zapisie lokalnych plików");
                return;
            }
        return;
        }
            if(option ==1){ //get

                String request = "getScore 1";
                ArrayList<String> rowsToSend = new ArrayList<>();
                ArrayList<String> rowsReceived = new ArrayList<>();

                pwToServer.println(request);
                pwToServer.flush();

                String lineToRead = brFromServer.readLine();
                if(!lineToRead.equals("sendScore 1")){
                    JOptionPane.showMessageDialog(this, "błąd nawiazaniu połączenia z serwerem 2");
                    return;
                }else{
                    while(!(lineToRead=brFromServer.readLine()).equals("*")){
                        rowsReceived.add(lineToRead);
                    }
                }
                System.out.println(game.rootPath+"highscore/highscore.properties");
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(game.rootPath+"config/highscore/highscore.properties"))){
                    for(int i = 0;i< rowsReceived.size();i++){
                        bw.write(rowsReceived.get(i));
                        bw.newLine();
                    }
                }catch(Exception e){
                    JOptionPane.showMessageDialog(this, "błąd w zapisie lokalnych plików");
                    return;
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "błąd nawiazaniu połączenia z serwerem 1");
            return;
        }
    }
}
