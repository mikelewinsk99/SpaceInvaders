package spaceinvaders;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Główna klasa wykonawcza gry
 * @author Michał Lewiński
 */
public class GamePanel extends JPanel implements KeyListener, Runnable{

    String version;

    private MainMenu menu;
    public void setMenu(MainMenu menu){this.menu=menu;}

        Player player;
        int vector;
        int playerSpeed = 1; //zmienna pomocnicza do lepszego sterowania graczem
    private final Set<Integer> pressedKeys = new HashSet<>();  //kolekcja do przetwarzania wielu klawiszy naraz

    String levelName;
    Enemy[] enemies;
    UFO ufo;
    Formation formation;
    BombGenerator bombGen;
    Bomb[] bombs;
    CannonBall cannonBall;
    int bulletTravelSpeed;

    int startingTickRateInMiliseconds;
    int currentTickRateInMiliseconds;

    int score = 0;

    Dimension board; //wymiar planszy
    boolean gameIsOn; //czy trwa rozgrywka
    boolean isPaused=false; //czy wcisnieto pauze

    boolean isRanked; // w trybie Arcade liczą się najwyższe wyniki
    int numberOfLevels, arcadeCurrentLevel;
    public void setArcadeCurrentLevel(int lv){this.arcadeCurrentLevel=lv;}
    public void setNumberOfLevels(int n){this.numberOfLevels=n;}


    public void gameOn(){gameIsOn = true;}
    public void gameOff(){gameIsOn = false;}


    public String getLevelName(){return this.levelName;}


    Properties core; //podstawowe ustawienia gry - geometria, HP itd.
    Properties level;

    int rows,columns,spacingX,spacingY,topLeftX,topLeftY;

    String rootPath,coreConfigPath,levelConfigPath;


    GamePanel() throws FileNotFoundException {
        //Załadowanie podstawowych parametrów gry

        rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        System.out.println("yeet"+rootPath);
        loadCore();  //ładowanie podstawowych rzeczy z core.properties

        addKeyListener(this);   //Utworzenie listenera dla inputu użytkownika (strzałki spacja itd)
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);

        this.setPreferredSize(new Dimension(400,400));
        this.setFocusable(true);
        Thread t = new Thread(this);
        t.start();
    }

    /**
     * metoda do wczytywania danych z pliku core.properties
     * do ładowania najważniejszych podstawowych parametrów:
     *
     *      -wersja gry - próba wczytania niekompatybilnej wersji mapy ma rzucać wyjątki
     *      -parametry definiujące obiekty (wymiary, hp uszkodzenia)
     *      -selekcja i kolejność dostępnych map(które gra udostępni użytkownikowi jeśli są wgrane dobre pliki)
     *
     */
    public void loadCore(){

        rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        System.out.println(rootPath);
        levelConfigPath = rootPath + "config/levels/";
        System.out.println(rootPath);
        coreConfigPath = rootPath + "config/core/";
        core = new Properties();
        try {
            core.load(new FileInputStream(coreConfigPath + "core.properties"));
        }catch(Exception e){System.out.println("nope");}

        System.out.println(core.getProperty("boardwidth")); // działa!

        version = core.getProperty("version");
        board = new Dimension(Integer.parseInt(core.getProperty("boardwidth")),Integer.parseInt(core.getProperty("boardheight")));
        bulletTravelSpeed = Integer.parseInt(core.getProperty("bulletTravelSpeed"));
        startingTickRateInMiliseconds = Integer.parseInt(core.getProperty("startingTickRateInMiliseconds"));
    }

    /**
     * metoda do wczytywania danych z aktualnie wybranej mapy
     * po wczytaniu wszystkich parametrów definiujących poziom
     * woła utworzenie wszystkich wątków gry i daje flagę pozwalającą kontynuować taktowanie w run()
     * @param mapName
     */
    public void loadMap(String mapName){

        player = new Player(this,0,0, board);  //utworzenie gracza

        level = new Properties();               //początek ładowania danych mapy
        try {
            level.load(new FileInputStream(levelConfigPath + mapName+".properties"));
        }catch(Exception e){System.out.println("nope2");}

        if(!level.getProperty("version").equals(version)){
            JOptionPane.showMessageDialog(this, "Ostrzeżenie, niekompatybilne wersje! ("+
                    level.getProperty("version") + " -> " + version +")\n" +
                    "Możliwe błędy rozgrywki\n" +
                    "Sprawdź pliki i uruchom grę ponownie:");
        }



        System.out.println(level.getProperty("playerStartingPointX")+level.getProperty("playerStartingPointY"));
        player.setXY(Integer.parseInt(level.getProperty("playerStartingPointX")),Integer.parseInt(level.getProperty("playerStartingPointY")));
        enemies = new Enemy[Integer.parseInt(level.getProperty("numberOfEnemies"))];
        System.out.println(enemies.length);
        levelName = level.getProperty("name");
        rows = Integer.parseInt(level.getProperty("numberOfRows"));
        columns = Integer.parseInt(level.getProperty("numberOfColumns"));
        spacingX = Integer.parseInt(level.getProperty("spacingX"));
        spacingY = Integer.parseInt(level.getProperty("spacingY"));
        topLeftX = Integer.parseInt(level.getProperty("topLeftX"));
        topLeftY = Integer.parseInt(level.getProperty("topLeftY"));

        menu.level.setText("    Level: " + levelName);  //wiadomosc do menu bocznego ktory poziom jest rozgrywany

        for(int i = 0; i<enemies.length;i++){      // pętla nanosząca przeciwników w formacji na planszę
            {
                for(int n = 1; n<=rows;n++){
                    String row = level.getProperty("row"+ Integer.toString(n));
                    for(int j = 0; j<columns;j++){
                        System.out.println(row);
                        if(row.charAt(j)!='0'){
                            enemies[i] = new Enemy(this,topLeftX + j*spacingX,topLeftY + n*spacingY, row.charAt(j));
                            enemies[i].setType(row.charAt(j)-48);  // -48 zgodnie z ASCII zeby znak 1 dal int 1
                            System.out.println(enemies[i].getType());
                            i++;
                        }
                    }
                }
            }
        }
        updateLives();
        formation = new Formation(this, board);   //nowe wątki
        bombGen = new BombGenerator(this);

        currentTickRateInMiliseconds = startingTickRateInMiliseconds; //dostosowanie taktowania PRZED gameIsOn
        //to jest najrozsadniejsze miejsce, bo zaraz zaczynamy kolejny poziom
        //był edge case w którym jak szybko klikano kolejny poziom to run nie zdążył wyjść z pętli gry i były błędy
        //dlatego flaga gameIsOn na SAM KONIEC
        gameIsOn = true;
        System.out.println("GAMEIS BACK ONNNNNNNNNNNNNNN!!!!");
    }

    /**
     * metoda będąca odwrotnością loadMap() - daje sygnał do przejścia run() w zewnętrzną pętlę uśpienia,
     * następnie usuwa pamięć o pozostałym po przeytym poziomie wątkach pomocniczych - te pozostawione same sobie
     * wychodzą ze swoich metod run
     */
    public void unloadMap(){
        gameIsOn=false; //jak najszybciej flaga żeby uśpić "główny wątek" gry w następnym "ticku"
        level=null;
        formation=null; //usun formacje

        //bez tej łatki przegrana przez lądowanie rujnowała kolejne poziomy - do usunięcia jak znajdę źródło problemu
        if(enemies!=null){
            for(int i=0;i<enemies.length;i++){ //usun przeciwników, żeby następne gry nie wczytywały starych obiektów
                enemies[i]=null;
            }
            enemies = null;
        }
        //koniec łatki

        //bombGen.terminateBombs();
        bombGen = null;
        cannonBall=null;
        System.out.println("GAMEIS OFFF!!!!");

    }

    /**
     * Główna pętla gry, składa się z zewnętrznej pętli oczekującej, i wewnętrznej do gry właściwej,
     * synchronizowanej zegarem taktującym i możliwością przełączenia pauzy,
     * obie pętle zawierają repaint(), a to co jest wyświetlane na ekrenie gry definiuje już logika
     * w paint()
     */
    @Override
    public void run() {
        System.out.println("Rozpoczalem prace");
        GameClock tickSignal = GameClock.getInstance();
        int currentTime = GameClock.getTime();
        while(true){
            repaint();
            //loadMap("B");
        currentTickRateInMiliseconds = startingTickRateInMiliseconds;
        while(gameIsOn) {

            synchronized (tickSignal) {
                tickSignal.notifyAll();
            }//(currentTime < lenSim)
            try {
                TimeUnit.MILLISECONDS.sleep(currentTickRateInMiliseconds);
                GameClock.tick();
                currentTime = GameClock.getTime();
                //System.out.println(currentTime);

            } catch (Exception e) {
                System.out.println(e);
            }

            movePlayer(vector);
            requestFocus();
            repaint();

            if(isPaused){
                synchronized(this) {

                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }


        }

            try {
                System.out.println("Spanko.....");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param g
     */
    public void paint(Graphics g){
        Graphics2D g2D = (Graphics2D) g;
        Dimension graphics = new Dimension(this.getSize().width,this.getSize().height);

        if(!gameIsOn) {
            g2D.setPaint(Color.black);
            g2D.fillRect(1, 1, this.getSize().width, this.getSize().height);
            g2D.setPaint(Color.WHITE);
            g2D.drawString("SPACE INVADERS",this.getSize().width/3, this.getSize().height/3);
        }
        else{
            g2D.setPaint(Color.black);        //namalowanie tła
            g2D.fillRect(1, 1, this.getSize().width, this.getSize().height);
            //g2D.setPaint(Color.darkGray);
            //g2D.fillRect(200*this.getSize().width/board.width,200*getSize().height/board.height,400*getSize().width/board.width,400*getSize().height/board.height);
            //g2D.drawLine(0,0,this.getSize().width, this.getSize().height);//pozostałość po testach skalowania ekranu
            g2D.setPaint(Color.white);
            g2D.drawLine(1,this.getSize().height*5/6,getSize().width,this.getSize().height*5/6);
            player.drawSelf(g2D,board, graphics);
            if(cannonBall!=null)
            cannonBall.drawSelf(g2D,board,graphics);

            if(enemies!=null) {
                for (int i = 0; i < enemies.length; i++) {
                    if (enemies[i] != null) {
                        enemies[i].drawSelf(g2D, board, graphics);
                        if (enemies[i].bomb != null)
                            enemies[i].bomb.drawSelf(g2D, board, graphics);
                    }
                }
            }
            try{
                for(int i=0;i<bombGen.getBombs().length;i++) {
                    if (bombGen.getBombs()[i] != null)
                        bombGen.getBombs()[i].drawSelf(g2D, board, graphics);
                }
            }catch(NullPointerException npe){System.out.println("Bomby się nie rysują, bo generator usunięto (czy zamykano mapę?)");}
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }


    @Override
    public synchronized void keyPressed(KeyEvent e) {
        pressedKeys.add(e.getKeyCode());
        vector=0;
        if (!pressedKeys.isEmpty()) {
            for (Iterator<Integer> it = pressedKeys.iterator(); it.hasNext();) {
                switch (it.next()) {
                    case KeyEvent.VK_LEFT: //IDZIEM W LEWO
                        vector = vector - 1;
                        break;
                    case KeyEvent.VK_RIGHT: //IDZIEM W PRAWO
                        vector = vector + 1;
                        break;
                    case 80: //tzn duże P (PAUZA)
                        System.out.println("kliknieto pause");
                        this.isPaused = (!this.isPaused);
                        if(!this.isPaused)
                            unpause();
                        break;
                    case 32: //spacja czyli STRZAL
                        System.out.println("kliknieto spacje");
                        if(cannonBall == null)
                            this.cannonBall = new CannonBall(this);
                        break;
                }
            }
        }
    }

    @Override
    public synchronized void keyReleased(KeyEvent e) {
        if(e.getKeyCode()== KeyEvent.VK_RIGHT || e.getKeyCode()== KeyEvent.VK_LEFT){
            playerSpeed=1; vector = 0;
        }
        pressedKeys.remove(e.getKeyCode());
    }


    /**
     * Bezpośrednie przesunięcie gracza o wektor przemieszczenia, wywoływane
     * co takt zegara gry. Gracz klawiszami steruje bezpośrednio parametrem vector i pośrednio
     * playerSpeed
     * @param vector
     */
    public void movePlayer(int vector){
        if(vector!=0) {
            if(playerSpeed<6) //jakiś realistyczny limit prędkości (dla dużego taktowania, trudny do opanowania)
                playerSpeed++;
            player.setPos_x(player.getPos_x() + vector * playerSpeed);
            if (player.getPos_x() > board.width)
                player.setPos_x(board.width - 1);
            if (player.getPos_x() < 1)
                player.setPos_x(1);
        }else playerSpeed=1;
}

    /**
     * Ukończenie poziomu woła o wyczyszczenie mapy, nawiązuje interakcje z użytkownikiem, sprawdza
     * czy zakończyć czy kontynuować w zależności od typu gry i postępu
     */
    public void completeLevel(){
        //gameOff(); //zawołanie tego wyrzuci run() w zewnętrzną pętle oczekującą, kiedy przeklikujemy okna dialogowe
        JOptionPane.showMessageDialog(this, "Ukończono poziom!");  //pogratuluj
        addScore(Integer.parseInt(level.getProperty("levelReward")));   //dodaj punkty za ukonczenie poziomu
       // Object[] possibilities = core.getProperty("levels").split(";");
        unloadMap();  //wyczyszc mape (usuwa też Properties level!)
        arcadeCurrentLevel++; //podnies o poziom
        if(!isRanked)  //jesli to treningowka to konczymy
        completeRun();
        else{
            numberOfLevels = Integer.parseInt(core.getProperty("numberOfLevels"));
            if(arcadeCurrentLevel>=numberOfLevels)
                completeRun();
            else{
                String[] levelChoice = core.getProperty("levels").split(";");
                loadMap(levelChoice[arcadeCurrentLevel]);
            }
        }
    }

    /**
     * Ta metoda wyrywa wątek gry z blokady w pętli aktywnej gry
     * Wywoływane klawiszem P
     */
    public synchronized void unpause(){
        notifyAll();
    }

    /**
     * Metoda kończąca całą rozgrywkę, jej zadaniem jest wywołanie unloadMap do czyszczenia planszy,
     * zinterpretowanie jaki tryb gry był prowadzony, wyświetlenie odpowiednich powiadomień i przejście
     * do podsumowania punktów
     */
    public void completeRun(){
        unloadMap();
        if(isRanked)
        {
            JOptionPane.showMessageDialog(this, "Ukończono bieg!\n" +
                    "Twój wynik to: "+ score);
            submitScore();
        }
        if(!isRanked){
            JOptionPane.showMessageDialog(this, "Trening zakończony!\n" +
                    "Twój wynik to: "+ score);
        }
        resetScore();
    }

    /**
     * Metoda ta jest wołana za każdym razem, gdy gracz zdobywa punkty, zwiększa sumaryczną ilość punktów
     * w pamięci gry i woła o odświeżenie je w panelu bocznym okna aplikacji.
     * Brak konfliktu write-xxx ponieważ gracz dysponuje tylko 1 pociskiem naraz synchronizacja niepotrzebna
     * @param points punkty, których dodania zarządano
     */
    public void addScore(int points)
    {
        score = score + points;
        menu.score.setText("    Score: " + score);
    }


    /**
     * Metoda wołana przez completeRun() dla trybu Arcade zajmuje się interakcją z użytkownikiem i ewentualnym
     * zapisaniem uzyskanych w biegu punktów do tabeli highscores jeśli wynik był dostateczny by się tam znaleźć
     */
    public void submitScore(){
        String name = JOptionPane.showInputDialog(("\"Wynik końcowy trybu Arcade: \n " +"Twój wynik to: "+ score + "\n"+
                "Podaj swój nick:"));

        String myLevel = levelName;
        int myScore = score;
        String myName = name;

        Properties hs = new Properties();
        try{
             hs.load(new FileInputStream(this.rootPath + "config/highscore/highscore.properties"));
         } catch (IOException e) {e.printStackTrace();}
        boolean flag = true;
        ArrayList<String> list =new ArrayList<>();
        String place=null;
        for(int i =1;i<=5;i++){
            String[] highscores = hs.getProperty(Integer.toString(i)).split(";");
            if(flag&&(myLevel.compareTo(highscores[0]) > 0 || highscores[0].compareTo(myLevel) == 0 && myScore> Integer.parseInt(highscores[1]))){
                flag = false;
                place = Integer.toString(i);
                list.add(myLevel+";"+myScore+";"+myName);
            }
                list.add(hs.getProperty(Integer.toString(i)));
        }
        for(int i =0;i<5;i++){
            hs.setProperty(Integer.toString(i+1),list.get(i));
        }
        try {
            hs.store(new FileOutputStream(this.rootPath + "config/highscore/highscore.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JOptionPane.showMessageDialog(this, "Zająłeś miejsce:" + place + "!!!");
    }

    /**
     * Zerowanie punktacji w pamięci gry i odświeżenie JPanel'a punktów
     */
    public void resetScore(){
        score = 0;
        menu.score.setText("    Score: " + score);
    }

    /**
     * Odświeżenie Jpanel'a pozostałego życia
     */
    public void updateLives(){
        String hp = "";
        for(int i =0;i<player.getHitpoints();i++){
            hp = hp + "X";
            menu.lives.setText("    Lives: " + hp);
        }
    }

    /**
     * Odpowiednik completeRun() dla osób, które przegrały rozgrywkę
     */
    public void gameOver(){
        unloadMap();
        JOptionPane.showMessageDialog(this, "Przegrales!\n" +
                "Twój wynik to: "+ score);
        if(isRanked){
            submitScore();
        }
        resetScore();
    }

    /**
     * Wołana jest przez wątek formacji po trafieniu obcego aby dostować taktowanie
     * do ilości obcych pozostałych przy życiu - gra przyspiesza
     * @param liveToAllRatio proporcja pozostalych przy życiu opcych do początkowej ich ilości
     */
    public void updateTickrate(double liveToAllRatio){
        this.currentTickRateInMiliseconds= (int)Math.ceil(((double)startingTickRateInMiliseconds*((1-0.3)*liveToAllRatio+0.3))); //wszyscy żyją = 100% okresu,  nikt nie zyje = "cokolwiek pod koniec nawiasu" okresu
    }
}
