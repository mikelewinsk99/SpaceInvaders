package spaceinvaders;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Klasa okna aplikacji SpaceInvaders
 * @author Michał Lewiński
 */

public class GameFrame extends JFrame {

    MainMenu mainmenu;
    GamePanel gamepanel;

    public GameFrame() {

        this.setSize(640, 640);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        try {
            gamepanel = new GamePanel();
        }catch(Exception e){System.out.println("Nie znaleziono pliku");};
        mainmenu = new MainMenu(gamepanel);
        gamepanel.setMenu(mainmenu);
        mainmenu.setPreferredSize(new Dimension(200, 900));
        this.setMinimumSize(new Dimension(270,270));
        this.add(gamepanel);
        this.add(mainmenu, BorderLayout.EAST);


        getContentPane().addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                Component c = (Component)e.getSource();
            }
        });
    }
}
