package spaceinvaders;

/**
 * Klasa zegara gry, do której wszystkie wątki biorące udział
 * w rozgrywce się synchronizują
 */

public class GameClock {

    // class used to represent simulated time

    private static GameClock instance = null;
    public static int simTime=0;

    /**
     * Metoda zapewnia możliwość uzyskania prawidłowej referencji na zegar gry przez dowolny obiekt
     * @return instancja obiektu GameClock utworzona jako pierwsza, a więc i jedyna
     */

public static synchronized GameClock getInstance(){
    if (instance == null) {
        instance = new GameClock();
    }
    return instance;
}

    public void SimClock() {
        simTime = 0;
    }

    public static synchronized void tick() {
        simTime ++;
    }

    public static synchronized int getTime(){
        return simTime;
    }
}